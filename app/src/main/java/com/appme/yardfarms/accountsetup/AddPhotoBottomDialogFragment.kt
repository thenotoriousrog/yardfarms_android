package com.appme.yardfarms.accountsetup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.appme.yardfarms.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.textview.MaterialTextView


class AddPhotoBottomDialogFragment : BottomSheetDialogFragment() {

    interface AddPhotoBottomSheetDialogListener {
        fun onTakePhotoClicked()
        fun onSelectPhotoClicked()
    }

    private lateinit var takePhotoTextView: MaterialTextView
    private lateinit var selectPhotoTextView: MaterialTextView
    private var listener: AddPhotoBottomSheetDialogListener? = null

    fun setAddPhotoBottomDialogListener(listener: AddPhotoBottomSheetDialogListener) {
        this.listener = listener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layout = inflater.inflate(R.layout.add_photo_dialog_fragment, container, false)

        this.takePhotoTextView = layout.findViewById(R.id.take_photo_text)
        takePhotoTextView.setOnClickListener {
            listener?.onTakePhotoClicked()
            this@AddPhotoBottomDialogFragment.dismiss()
        }

        this.selectPhotoTextView = layout.findViewById(R.id.select_photo_text)
        selectPhotoTextView.setOnClickListener {
            listener?.onSelectPhotoClicked()
            this@AddPhotoBottomDialogFragment.dismiss()
        }

        return layout
    }
}