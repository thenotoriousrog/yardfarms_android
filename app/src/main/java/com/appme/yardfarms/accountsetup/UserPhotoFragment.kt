package com.appme.yardfarms.accountsetup

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.FileProvider
import com.appme.yardfarms.CircularImageView
import com.appme.yardfarms.HomeActivity
import com.appme.yardfarms.R
import com.appme.yardfarms.SnackbarMessage
import com.appme.yardfarms.backend.Constants
import com.appme.yardfarms.backend.persistance.FirebaseHelper
import com.appme.yardfarms.util.Utils
import com.appme.yardfarms.util.edit
import com.appme.yardfarms.util.launch
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageOptions
import com.theartofdev.edmodo.cropper.CropImageView
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.File

class UserPhotoFragment : AccountSetupFragment(), UserPhotoFragmentUpdater, AddPhotoBottomDialogFragment.AddPhotoBottomSheetDialogListener {

    companion object {
        private const val ADD_PHOTO_DIAL_FRAG_TAG = "add_photo_bottom_dialog_fragment"
        private const val SELECT_PHOTO_INTENT_TITLE = "Select Profile Image"
        const val TAKE_PHOTO_CAPTURE_CODE = 1000
        const val SELECT_PHOTO_CODE = 2000
    }

    private lateinit var addPhotoImageView: CircularImageView
    private lateinit var skipButton: MaterialButton
    private val addPhotoBottomDialogFragment = AddPhotoBottomDialogFragment()
    private val presenter by lazy { UserPhotoFragmentPresenter(this) }

    init {
        addPhotoBottomDialogFragment.setAddPhotoBottomDialogListener(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layout = inflater.inflate(R.layout.user_photo_fragment, container, false)

        this.skipButton = layout.findViewById(R.id.skip_button)
        skipButton.setOnClickListener {
            nextButtonClicked()
        }

        this.addPhotoImageView = layout.findViewById(R.id.add_photo_image_view)
        addPhotoImageView.setOnClickListener {
            addPhotoBottomDialogFragment.show(childFragmentManager, ADD_PHOTO_DIAL_FRAG_TAG)
        }

        return layout
    }

    override fun onTakePhotoClicked() {

        val photoFile = presenter.createImageFile()

        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->

            if (photoFile != null) {
                val photoUri = FileProvider.getUriForFile(accountSetupActivity, Constants.FILE_PROVIDER_AUTHORITY, photoFile)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
            }

            takePictureIntent.resolveActivity(accountSetupActivity.packageManager)?.also {
                startActivityForResult(takePictureIntent, TAKE_PHOTO_CAPTURE_CODE)
            }
        }

    }

    override fun onSelectPhotoClicked() {
        val getIntent = Intent(Intent.ACTION_GET_CONTENT)
        getIntent.type = "image/*"

        val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickIntent.type = "image/*"

        val chooserIntent = Intent.createChooser(getIntent, SELECT_PHOTO_INTENT_TITLE)
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

        startActivityForResult(pickIntent, SELECT_PHOTO_CODE)
    }

    private fun processPhotoCaptureRequest() {
        val lastFile = presenter.lastCreatedFile
        val uri = Uri.fromFile(lastFile)
        if(uri == null) {
            Timber.e("No Uri can be obtained from user's photo")
            accountSetupActivity.displaySnackbarMessage(R.string.take_photo_processing_error, Snackbar.LENGTH_SHORT, SnackbarMessage.Error())
        } else {
            CropImage.activity(uri).setCropShape(CropImageView.CropShape.OVAL).start(accountSetupActivity, this)
        }
    }

    private fun processSelectPhotoRequest(data: Intent?) {
        val uri = data?.data

        if(uri == null) {
            accountSetupActivity.displaySnackbarMessage(R.string.select_photo_processing_error, Snackbar.LENGTH_SHORT, SnackbarMessage.Error())
        } else {
            CropImage.activity(uri).setCropShape(CropImageView.CropShape.OVAL).start(accountSetupActivity, this)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == TAKE_PHOTO_CAPTURE_CODE) {
            if(resultCode != Activity.RESULT_OK && resultCode != Activity.RESULT_CANCELED) {
                accountSetupActivity.displaySnackbarMessage(R.string.take_photo_error, Snackbar.LENGTH_SHORT, SnackbarMessage.Error())
                Timber.e("Error while attempting to take photo!")
                return
            } else {
                processPhotoCaptureRequest()
            }
        }
        else if(requestCode == SELECT_PHOTO_CODE) {
            if(resultCode != Activity.RESULT_OK && resultCode != Activity.RESULT_CANCELED) {
                accountSetupActivity.displaySnackbarMessage(R.string.select_photo_error, Snackbar.LENGTH_SHORT, SnackbarMessage.Error())
                Timber.e("Encountered error while attempting to select photo")
            } else {
                processSelectPhotoRequest(data)
            }
        } else if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if(resultCode == Activity.RESULT_OK) {
                this.addPhotoImageView.setImageURI(result.uri)

                val bitmap = Utils.getBitmapFromUri(result.uri, accountSetupActivity)
                presenter.saveProfilePicture(bitmap)
            }
        }
    }

    override fun onBackPressed() {
        if(addPhotoBottomDialogFragment.isVisible) {
            addPhotoBottomDialogFragment.dismiss()
        } else {
            prevButtonClicked()
        }
    }

    override fun getExternalFilesDirectory(dir: String): File? {
        return accountSetupActivity.getExternalFilesDir(dir)
    }

    override fun nextButtonClicked() {
        accountSetupActivity.generalSharedPrefs.edit {
            putBoolean(Constants.PREFS_HAS_FINISHED_ACCOUNT_SETUP, true)
        }

        startActivity(HomeActivity.getNewIntent(accountSetupActivity))
        accountSetupActivity.finish()
    }

    override fun prevButtonClicked() {
        accountSetupActivity.startUserBankInfoFragment()
    }
}