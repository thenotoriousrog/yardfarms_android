package com.appme.yardfarms.accountsetup

import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment

/**
 * This is the base fragment through which all other Fragments are based off of
 */
abstract class AccountSetupFragment : Fragment() {

    /**
     * Allows the fragment to handle back pressed events. This will override the activity's
     * back pressed behavior
     */
    var allowBackPress = false
        set(value) {
            field = value

            if(value) {
                view?.setOnKeyListener { view, keyCode, keyEvent ->
                    return@setOnKeyListener if (keyCode == KeyEvent.KEYCODE_BACK) {
                        onBackPressed()
                        true
                    } else {
                        false
                    }
                }
            } else {
                view?.setOnClickListener(null)
            }
        }


    protected val accountSetupActivity by lazy { context as AccountSetupActivity }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        require(activity is AccountSetupActivity) { "All AccountSetupFragments must be used with AccountSetupActivity!" }
    }

    /**
     * Alerts the fragment that the user has pressed the back button. This will only fire
     * if the fragment is allowed to handle back pressed events which is turned on through
     * variable allowBackPress
     */
    abstract fun onBackPressed()

    /**
     * Reports that the user pressed nextButton in the AccountSetupActivity
     */
    abstract fun nextButtonClicked()

    /**
     * Reports that the user pressed prevButton in the AccountSetupActivity
     */
    abstract fun prevButtonClicked()
}