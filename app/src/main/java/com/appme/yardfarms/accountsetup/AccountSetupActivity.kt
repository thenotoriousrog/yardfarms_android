package com.appme.yardfarms.accountsetup

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import com.appme.yardfarms.BaseActivity
import com.appme.yardfarms.R
import com.google.android.material.button.MaterialButton

class AccountSetupActivity : BaseActivity() { // **Note: AccountSetupActivity Doesn't have a need for a presenter as of yet.

    private lateinit var fragmentContainer: FrameLayout
    private lateinit var backButton: MaterialButton
    private lateinit var nextButton: MaterialButton
    private lateinit var currentFragment: AccountSetupFragment

    override val layoutResId: Int
        get() = R.layout.account_setup_activity

    override val snackbarContainerView: View
        get() = fragmentContainer


    fun startUserInfoFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        val userInfoFragment = UserInfoFragment()
        this.currentFragment = userInfoFragment
        userInfoFragment.allowBackPress = true
        transaction.replace(R.id.fragment_container, userInfoFragment)
        transaction.commit()
        supportActionBar?.title = getString(R.string.user_info_fragment_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        backButton.text = getString(R.string.close)
        nextButton.text = getString(R.string.next)
    }

    fun startUserBankInfoFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        val bankInfoFragment = UserBankInfoFragment()
        this.currentFragment = bankInfoFragment
        bankInfoFragment.allowBackPress = true
        transaction.replace(R.id.fragment_container, bankInfoFragment)
        transaction.commit()
        supportActionBar?.title = getString(R.string.user_bank_info_fragment_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        backButton.text = getString(R.string.back)
        nextButton.text = getString(R.string.next)
    }

    fun startUserPhotoFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        val userPhotoFragment = UserPhotoFragment()
        this.currentFragment = userPhotoFragment
        userPhotoFragment.allowBackPress = true
        transaction.replace(R.id.fragment_container, userPhotoFragment)
        transaction.commit()
        supportActionBar?.title = getString(R.string.user_photo_fragment_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        backButton.text = getString(R.string.back)
        nextButton.text = getString(R.string.finish)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.fragmentContainer = findViewById(R.id.fragment_container)
        this.backButton = findViewById(R.id.back_button)
        backButton.setOnClickListener {
            currentFragment.prevButtonClicked()
        }

        this.nextButton = findViewById(R.id.next_button)
        nextButton.setOnClickListener {
            currentFragment.nextButtonClicked()
        }

        startUserInfoFragment()
    }

    override fun onSupportNavigateUp(): Boolean {
        currentFragment.prevButtonClicked()
        return true
    }

    fun enableButtons() {
        this.nextButton.isEnabled = true
        this.backButton.isEnabled = true
    }

    fun disableButtons() {
        this.nextButton.isEnabled = false
        this.backButton.isEnabled = false
    }

}