package com.appme.yardfarms.accountsetup

import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import com.appme.yardfarms.backend.UserInfo
import com.appme.yardfarms.backend.persistance.FirebaseHelper
import com.appme.yardfarms.backend.persistance.listeners.FirebaseCallback
import com.appme.yardfarms.backend.persistance.listeners.FirebaseItemRetriever
import com.google.firebase.database.DatabaseError
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class UserPhotoFragmentPresenter(private val updater: UserPhotoFragmentUpdater) {

    var lastCreatedFile: File? = null

    fun createImageFile(): File? {

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())

        val imageFileName = "IMG_" + timeStamp + "_"

        val storageDir = updater.getExternalFilesDirectory(Environment.DIRECTORY_PICTURES)

        val imageFile =  try {
            File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir    // directory
            )
        } catch (ex: IOException) {
            Timber.e(ex, "Caught exception while creating temporary image file")
            null
        }

        this.lastCreatedFile = imageFile
        return imageFile
    }

    private fun updateCurrentUserProfilePicture(uri: Uri) {
        val currentUserInfo = FirebaseHelper.currentUserInfo
        if(currentUserInfo != null) {
            currentUserInfo.profilePicture = uri.toString()
            FirebaseHelper.updateUser(currentUserInfo)
        } else {
            FirebaseHelper.getCurrentUser(object : FirebaseItemRetriever<UserInfo?> {
                override fun onItemRetrieved(item: UserInfo?) {
                    if(item == null) {
                        Timber.e("UserInfo was retrieved successfully from Firebase but was null!")
                    } else {
                        item.profilePicture = uri.toString()
                        FirebaseHelper.updateUser(item)
                    }
                }

                override fun onError() {
                    Timber.e("Unable to update the current user's profile picture because it was not found in Firebase")
                }
            })
        }
    }

    fun saveProfilePicture(bitmap: Bitmap) {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)

        FirebaseHelper.updateProfilePicture(baos.toByteArray(), object : FirebaseCallback {
            override fun onCancel() { }

            override fun onError(exception: Exception?) { }

            override fun onDatabaseError(databaseError: DatabaseError) { }

            override fun onSuccess(item: Any?) {
                if(item is Uri) {
                    updateCurrentUserProfilePicture(item)
                }
            }
        })
    }
}