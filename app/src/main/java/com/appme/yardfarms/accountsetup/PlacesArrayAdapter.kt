package com.appme.yardfarms.accountsetup

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.annotation.LayoutRes
import com.appme.yardfarms.R
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.material.textview.MaterialTextView
import timber.log.Timber

/**
 * Click listener Used to return the exact AutocompletePrediction that the user has chosen
 */
typealias OnPlaceClickedListener = (AutocompletePrediction) -> Unit

/**
 * Custom Adapter that allows us to pass in the Powered by Google attribution
 */
class PlacesArrayAdapter(
    context: Context,
    @LayoutRes private val layout: Int,
    private var items: List<AutocompletePrediction>) : ArrayAdapter<AutocompletePrediction>(context, layout, items) {

    private val inflater = LayoutInflater.from(context)
    private var placesClickListener: OnPlaceClickedListener? = null

    fun setPlacesClickListener(placesClickListener: OnPlaceClickedListener) {
        this.placesClickListener = placesClickListener
    }

    override fun getCount(): Int {
        return items.size
    }

    fun updateItems(newItems: List<AutocompletePrediction>) {
        this.items = newItems
        addAll(newItems)
    }

    override fun getItem(position: Int): AutocompletePrediction? {
        return if(position - 1 >= items.size ) {
            items[position]
        } else {
            null
        }
    }

    override fun clear() {
        super.clear()
        items = ArrayList() // set to a new list
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        if(position == 0) {
            return inflater.inflate(R.layout.places_attribution_layout, parent, false)
        }

        val view = inflater.inflate(R.layout.places_layout, parent, false)
        val textView: MaterialTextView = view.findViewById(R.id.places_address_text)
        textView.text = items[position].getFullText(null).toString()

        view.setOnClickListener {
            val placeClicked = items[position]
            placesClickListener?.invoke(placeClicked)
        }

        return view
    }
}