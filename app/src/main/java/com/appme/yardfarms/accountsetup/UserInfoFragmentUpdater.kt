package com.appme.yardfarms.accountsetup

import androidx.annotation.StringRes
import com.appme.yardfarms.SnackbarMessage
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.net.PlacesClient

interface UserInfoFragmentUpdater {

    fun displayProgressBar()

    fun hideProgressBar()

    /**
     * Places client object for interacting with addresses
     * Null if there was an error generating a proper PlacesClient object
     */
    val placesClient: PlacesClient?

    /**
     * Requests a snackbar message to be displayed by the host activity.
     */
    fun displaySnackbar(@StringRes message: Int, length: Int, messageType: SnackbarMessage.Type)

    /**
     * Called when the list of items has been validated and ready to be shown to the user
     * @param items - the items to display to the user
     */
    fun updateAddressItems(items: List<AutocompletePrediction>)

    /**
     * Displays an error under the name text field
     */
    fun showNameTextError(@StringRes errorMsg: Int)

    /**
     * Displays an error under the age text field
     */
    fun showAgeTextError(@StringRes errorMsg: Int)

    /**
     * Displays an error under the address text field
     */
    fun showAddressTextError(@StringRes errorMsg: Int)

    fun disableViews()

    fun enableViews()

    fun startNextFragment()

}