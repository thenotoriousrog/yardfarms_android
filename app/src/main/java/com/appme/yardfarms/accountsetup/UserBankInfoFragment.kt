package com.appme.yardfarms.accountsetup

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.appme.yardfarms.R
import com.google.android.material.button.MaterialButton
import java.io.File

class UserBankInfoFragment : AccountSetupFragment(), UserBankInfoFragmentUpdater {

    private lateinit var skipButton: MaterialButton

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layout = inflater.inflate(R.layout.user_bank_info_fragment, container, false)

        this.skipButton = layout.findViewById(R.id.skip_button)
        skipButton.setOnClickListener {
            nextButtonClicked()
        }

        return layout
    }

    override fun onBackPressed() {
        prevButtonClicked()
    }

    override fun nextButtonClicked() {
        accountSetupActivity.startUserPhotoFragment()
    }

    override fun prevButtonClicked() {
        accountSetupActivity.startUserInfoFragment()
    }

}