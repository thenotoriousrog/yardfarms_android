package com.appme.yardfarms.accountsetup

import java.io.File

interface UserPhotoFragmentUpdater {

    fun getExternalFilesDirectory(dir: String): File?

}