package com.appme.yardfarms.accountsetup

import com.appme.yardfarms.R
import com.appme.yardfarms.SnackbarMessage
import com.appme.yardfarms.backend.Address
import com.appme.yardfarms.backend.UserInfo
import com.appme.yardfarms.backend.persistance.FirebaseHelper
import com.appme.yardfarms.backend.persistance.listeners.FirebaseCallback
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DatabaseError
import timber.log.Timber

class UserInfoFragmentPresenter(private val updater: UserInfoFragmentUpdater) {

    private val autoCompleteSessionToken = AutocompleteSessionToken.newInstance()

    /**
     * The place that the user has officially selected!
     */
    var selectedPlace: AutocompletePrediction? = null

    private var addressHasPrediction = false // tells the system that the user's address has a prediction they could click. Used for error handling

    private var hasShownClickPredictionRecommendation = false // tells the presenter that we've recommended the user to click a prediction for their address

    /**
     * Handles the error code when the Places API fails and determines the appropriate course of action
     */
    private fun handlePlacesError(errorCode: Int) {
        when(errorCode) {
            CommonStatusCodes.API_NOT_CONNECTED or CommonStatusCodes.DEVELOPER_ERROR or
                    CommonStatusCodes.ERROR or CommonStatusCodes.RESOLUTION_REQUIRED or
                    CommonStatusCodes.INTERNAL_ERROR or CommonStatusCodes.INVALID_ACCOUNT or
                    CommonStatusCodes.SIGN_IN_REQUIRED -> updater.displaySnackbar(R.string.address_search_failure, Snackbar.LENGTH_LONG, SnackbarMessage.Type.ERROR)

            CommonStatusCodes.NETWORK_ERROR -> updater.displaySnackbar(R.string.general_network_error, Snackbar.LENGTH_SHORT, SnackbarMessage.Type.PRIORITY)

            else -> Unit // do nothing, the remaining error codes can be resolved on their own or there's nothing we can do
        }
    }

    /**
     * Retrieves a max of 5 total addresses. If there is not 5 addresses to show then the max amount is
     * returned that's available
     */
    private fun getFoundAddresses(foundAddresses: List<AutocompletePrediction>): List<AutocompletePrediction> {
        val toReturn = ArrayList<AutocompletePrediction>()
        if(foundAddresses.size < 5) {
            for(index in foundAddresses.indices) {
                toReturn.add(foundAddresses[index])
            }
        } else {
            for(index in 0..4) {
                toReturn.add(foundAddresses[index])
            }
        }

        return toReturn
    }

    /**
     * Alerts the presenter that the user has begun adding their address
     */
    fun textUpdated(enteredText: CharSequence?) {
        if(enteredText != null) {
            val predictionRequest = FindAutocompletePredictionsRequest.builder()
                .setTypeFilter(TypeFilter.ADDRESS)
                .setSessionToken(autoCompleteSessionToken)
                .setQuery(enteredText.toString())
                .build()

            val placesClient = updater.placesClient ?: return // we're done if the user doesn't have a places client

            placesClient.findAutocompletePredictions(predictionRequest).addOnSuccessListener { response ->
                this.addressHasPrediction = response.autocompletePredictions.size > 0
                updater.updateAddressItems(getFoundAddresses(response.autocompletePredictions))

            }.addOnFailureListener { exception ->
                val ex = exception as ApiException
                Timber.e(ex,"Encountered error attempting to find user's address! Text entered: $enteredText status code: ${ex.statusCode}")
                handlePlacesError(ex.statusCode)
            }
        }
    }

    /**
     * Sends the information to Firebase to be saved
     */
    private fun sendUserData(userInfo: UserInfo) {
        updater.displayProgressBar()
        updater.disableViews()

        FirebaseHelper.updateUser(userInfo, object : FirebaseCallback {
            override fun onCancel() {
                updater.displaySnackbar(R.string.general_error, Snackbar.LENGTH_LONG, SnackbarMessage.Type.ERROR)
                updater.hideProgressBar()
                updater.enableViews()
            }

            override fun onError(exception: Exception?) {
                updater.displaySnackbar(R.string.general_error, Snackbar.LENGTH_LONG, SnackbarMessage.Type.ERROR)
                updater.hideProgressBar()
                updater.enableViews()
            }

            override fun onDatabaseError(databaseError: DatabaseError) { } // not needed

            override fun onSuccess(item: Any?) {
                updater.hideProgressBar()
                updater.enableViews()
                updater.startNextFragment()
            }
        })
    }

    /**
     * Verifies all of the information that the user has entered.
     * If all of the information is valid, it saves the information to firebase.
     */
    fun saveUserData(name: String?, age: String?, bio: String?,
                     addressEntered: String?, unitNumber: String?) {

        var isValid = true

        if (name.isNullOrBlank()) {
            isValid = false
            updater.showNameTextError(R.string.enter_name_error)
        }
        if (age.isNullOrBlank()) {
            isValid = false
            updater.showAgeTextError(R.string.enter_age_error)
        }
        if (selectedPlace == null && addressEntered.isNullOrBlank()) {
            isValid = false
            updater.showAddressTextError(R.string.enter_address_error)
        }
        if (selectedPlace == null && !addressEntered.isNullOrBlank() && addressHasPrediction && !hasShownClickPredictionRecommendation) {
            isValid = false
            hasShownClickPredictionRecommendation = true
            updater.displaySnackbar(R.string.use_prediction_warning, Snackbar.LENGTH_LONG, SnackbarMessage.Type.SECONDARY)
        }

        if(!isValid) return // we're done until user resubmits

        // generate user information
        val streetAddress = selectedPlace?.getFullText(null) ?: addressEntered!! // should be impossible for addressEntered to be null here
        val placesId = selectedPlace?.placeId

        val address = Address(placesId, streetAddress.toString(), unitNumber)
        val userInfo = UserInfo(userId = FirebaseHelper.userId,
                                name = name!!,
                                age = age!!,
                                email = FirebaseHelper.userEmail,
                                bio =  bio,
                                profilePicture = null, // **Note: This will be changed later in the signup process
                                address = address)

        sendUserData(userInfo)

    }

}