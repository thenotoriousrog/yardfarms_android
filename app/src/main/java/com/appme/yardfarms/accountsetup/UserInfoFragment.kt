package com.appme.yardfarms.accountsetup

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.fragment.app.Fragment
import com.appme.yardfarms.R
import com.appme.yardfarms.SnackbarMessage
import com.appme.yardfarms.util.dismissKeyboard
import com.appme.yardfarms.util.toGone
import com.appme.yardfarms.util.toVisible
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.material.textfield.TextInputLayout
import timber.log.Timber

class UserInfoFragment : AccountSetupFragment(), UserInfoFragmentUpdater {

    companion object {
        private const val USER_NAME_TAG = "tag_user_name"
        private const val USER_AGE_TAG = "tag_user_age"
        private const val USER_BIO_TAG = "tag_user_bio"
        private const val USER_SELECTED_PLACE_ADDRESS_TAG = "tag_user_selected_place_address"
        private const val USER_SELECTED_PLACE_ID_TAG = "tag_user_selected_place_id"
        private const val USER_UNIT_NUMBER_TAG = "tag_user_unit_number"
    }

    private val presenter by lazy { UserInfoFragmentPresenter(this) }

    private lateinit var userNameLayout: TextInputLayout
    private lateinit var userAgeLayout: TextInputLayout
    private lateinit var userBioLayout: TextInputLayout
    private lateinit var userAddress: AppCompatAutoCompleteTextView
    private lateinit var userUnitNumberLayout: TextInputLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var addressArrayAdapter: PlacesArrayAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val layout = inflater.inflate(R.layout.user_info_fragment_layout, container, false)

        this.userNameLayout = layout.findViewById(R.id.user_name_layout)
        this.userAgeLayout = layout.findViewById(R.id.user_age_layout)
        this.userBioLayout = layout.findViewById(R.id.user_bio_layout)
        this.userAddress = layout.findViewById(R.id.user_address_auto_complete_text_view)

        userAddress.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(afterTextChanged: Editable?) { }
            override fun beforeTextChanged(str: CharSequence?, start: Int, count: Int, after: Int) { }
            override fun onTextChanged(str: CharSequence?, start: Int, before: Int, count: Int) {
                presenter.textUpdated(str)
            }
        })

        this.userUnitNumberLayout = layout.findViewById(R.id.user_unit_number_layout)

        this.progressBar = layout.findViewById(R.id.user_info_progress_bar)
        this.addressArrayAdapter = PlacesArrayAdapter(accountSetupActivity, R.layout.places_layout, ArrayList())
        addressArrayAdapter.setNotifyOnChange(false)
        userAddress.threshold = 1
        userAddress.setAdapter(addressArrayAdapter)

        addressArrayAdapter.setPlacesClickListener { prediction ->
            presenter.selectedPlace = prediction
            if (userAddress.isPopupShowing) {
                userAddress.dismissDropDown()
                userAddress.setText(prediction.getFullText(null).toString())
            }
        }

        return layout
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(USER_NAME_TAG, userNameLayout.editText?.text.toString())
        outState.putString(USER_AGE_TAG, userAgeLayout.editText?.text.toString())
        outState.putString(USER_BIO_TAG, userBioLayout.editText?.text.toString())
        val selectedPlace = presenter.selectedPlace
        if(selectedPlace != null) {
            outState.putString(USER_SELECTED_PLACE_ADDRESS_TAG, selectedPlace.getFullText(null).toString())
            outState.putString(USER_SELECTED_PLACE_ID_TAG, selectedPlace.placeId)
        } else {
            outState.putString(USER_SELECTED_PLACE_ADDRESS_TAG, userAddress.text.toString())
            outState.putString(USER_SELECTED_PLACE_ID_TAG, null)
        }
        outState.putString(USER_UNIT_NUMBER_TAG, userUnitNumberLayout.editText?.text.toString())
    }


    override fun nextButtonClicked() {
        val name = userNameLayout.editText?.text.toString()
        val age = userAgeLayout.editText?.text.toString()
        val bio = userBioLayout.editText?.text.toString()
        val address = userAddress.text.toString()
        val unitNumber = userUnitNumberLayout.editText?.text.toString()
        disableErrors()
        presenter.saveUserData(name, age, bio, address, unitNumber)
    }

    override fun prevButtonClicked() {
        accountSetupActivity.finish() // nothing to do here except kill off the activity. The user must go through account setup
    }

    override fun onBackPressed() {
        if(userAddress.isPopupShowing) {
            context?.dismissKeyboard(view)
            userAddress.showDropDown()
        } else {
            prevButtonClicked()
        }
    }

    override fun displayProgressBar() {
        progressBar.toVisible()
    }

    override fun hideProgressBar() {
        progressBar.toGone()
    }

    override val placesClient: PlacesClient?
        get() {
            return if(context != null) {
                Places.createClient(context!!)
            } else {
                Timber.e("Unable to create a valid Places Client! Address predictions will not be possible")
                // TODO: Add error behavior. Let the user manually type their address? Make them wait? Alert them and don't double check their address?
                null
            }
        }

    override fun displaySnackbar(message: Int, length: Int, messageType: SnackbarMessage.Type) {
        accountSetupActivity.displaySnackbarMessage(message, length, SnackbarMessage.getFromType(messageType))
    }

    override fun updateAddressItems(items: List<AutocompletePrediction>) {
        addressArrayAdapter.clear()
        addressArrayAdapter.updateItems(items)
        addressArrayAdapter.notifyDataSetChanged()
    }

    private fun disableErrors() {
        userNameLayout.isErrorEnabled = false
        userAgeLayout.isErrorEnabled = false
        userAddress.error = null
    }

    override fun showNameTextError(errorMsg: Int) {
        userNameLayout.error = getString(errorMsg)
        userNameLayout.isErrorEnabled = true
    }

    override fun showAgeTextError(errorMsg: Int) {
        userAgeLayout.error = getString(errorMsg)
        userAgeLayout.isErrorEnabled = true
    }

    override fun showAddressTextError(errorMsg: Int) {
        userAddress.error  = getString(errorMsg)
    }

    override fun disableViews() {
        accountSetupActivity.disableButtons()
        userNameLayout.isEnabled = false
        userAgeLayout.isEnabled = false
        userBioLayout.isEnabled = false
        userUnitNumberLayout.isEnabled = false
        userAddress.isEnabled = false
    }

    override fun enableViews() {
        accountSetupActivity.enableButtons()
        userNameLayout.isEnabled = true
        userAgeLayout.isEnabled = true
        userBioLayout.isEnabled = true
        userUnitNumberLayout.isEnabled = true
        userAddress.isEnabled = true
    }

    override fun startNextFragment() {
        accountSetupActivity.startUserBankInfoFragment()
    }
}