package com.appme.yardfarms.authentication

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.constraintlayout.widget.ConstraintLayout
import com.appme.yardfarms.BaseActivity
import com.appme.yardfarms.R
import com.appme.yardfarms.SnackbarMessage
import com.appme.yardfarms.util.toGone
import com.appme.yardfarms.util.toVisible
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout

class ForgotPasswordActivity : BaseActivity(), ForgotPasswordActivityUpdater {

    private val presenter = ForgotPasswordActivityPresenter(this)
    private lateinit var container: ConstraintLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var recoveryEmailLayout: TextInputLayout
    private lateinit var sendRecoveryEmailButton: MaterialButton

    override val layoutResId: Int
        get() = R.layout.forgot_password_activity_layout

    override val snackbarContainerView: View
        get() = container

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.title = getString(R.string.forgot_password_activity_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        this.container = findViewById(R.id.forgot_password_activity_container)
        this.progressBar = findViewById(R.id.progress_bar)
        this.recoveryEmailLayout = findViewById(R.id.recovery_email_layout)
        this.sendRecoveryEmailButton = findViewById(R.id.send_recovery_email_button)
        sendRecoveryEmailButton.setOnClickListener {
            presenter.recoverPassword(recoveryEmailLayout.editText?.text.toString())
        }
    }

    override fun displayEmailError(error: Int) {
        recoveryEmailLayout.error = getString(error)
        recoveryEmailLayout.isErrorEnabled = true
    }

    override fun disableSendRecoveryEmailButton() {
        sendRecoveryEmailButton.isEnabled = false
    }

    override fun enableSendRecoveryEmailButton() {
        sendRecoveryEmailButton.isEnabled = true
    }

    override fun showProgressBar() {
        progressBar.toVisible()
    }

    override fun hideProgressBar() {
        progressBar.toGone()
    }

    override fun displaySnackbar(message: Int, length: Int, messageType: SnackbarMessage.Type) {
        displaySnackbarMessage(message, length, messageType = SnackbarMessage.getFromType(messageType))
    }

    override fun terminate() {
        finish()
    }
}