package com.appme.yardfarms.authentication

import com.appme.yardfarms.R
import com.appme.yardfarms.SnackbarMessage
import com.appme.yardfarms.backend.persistance.FirebaseHelper
import com.appme.yardfarms.backend.persistance.listeners.FirebaseCallback
import com.appme.yardfarms.util.isValidEmail
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DatabaseError

class ForgotPasswordActivityPresenter(private val updater: ForgotPasswordActivityUpdater) {

    fun recoverPassword(email: String?) {
        updater.disableSendRecoveryEmailButton()
        updater.showProgressBar()
        if( email.isNullOrBlank() || !email.isValidEmail()) {
            updater.displayEmailError(R.string.enter_password_error)
            updater.hideProgressBar()
            updater.enableSendRecoveryEmailButton()
            return
        }

        FirebaseHelper.sendPasswordResetEmail(email, object : FirebaseCallback {
            override fun onCancel() {
                updater.hideProgressBar()
                updater.enableSendRecoveryEmailButton()
            }

            override fun onError(exception: Exception?) {
                updater.displaySnackbar(R.string.forgot_password_recovery_email_error, Snackbar.LENGTH_LONG, SnackbarMessage.Type.ERROR)
                updater.hideProgressBar()
                updater.enableSendRecoveryEmailButton()
            }

            override fun onDatabaseError(databaseError: DatabaseError) {} // not needed

            override fun onSuccess(item: Any?) {
                updater.hideProgressBar()
                updater.terminate()
            }
        })


    }

}