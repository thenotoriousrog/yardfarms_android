package com.appme.yardfarms.authentication.biometrics

interface BiometricSignUpListener {

    /**
     * Called when the user's information was encrypted correctly.
     * Important because it's will help determine the app behavior going forward
     */
    fun onEncryptionSuccess()

    /**
     * Something has caused the user to not be logged in using biometric's. This error can be fired by
     * LoginAuthenticator or by Biometric api.
     * User will have to log in manually.
     * @param failure - the type of failure that is returned.
     * @param errString - an error string that is being returned. This is often returned when triggered by an error called
     * by the Biometric api. Null is often passed if triggered by LoginAuthenticator but not gauranteed.
     */
    fun onAuthFailure(failure: AuthFailure, errString: CharSequence? = null)

}