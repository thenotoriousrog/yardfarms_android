package com.appme.yardfarms.authentication

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.biometric.BiometricPrompt
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.appme.yardfarms.BaseActivity
import com.appme.yardfarms.R
import com.appme.yardfarms.SnackbarMessage
import com.appme.yardfarms.accountsetup.AccountSetupActivity
import com.appme.yardfarms.util.launch
import com.appme.yardfarms.util.toGone
import com.appme.yardfarms.util.toVisible
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import timber.log.Timber

class SignUpActivity : BaseActivity(), SignUpActivityUpdater {

    private lateinit var signupContainer: ConstraintLayout
    private lateinit var enterEmailLayout: TextInputLayout
    private lateinit var enterPasswordLayout: TextInputLayout
    private lateinit var confirmPasswordLayout: TextInputLayout
    private lateinit var signupButton: MaterialButton
    private lateinit var progressBar: ProgressBar
    private val presenter by lazy { SignUpActivityPresenter(this, generalSharedPrefs) }
    private val executor by lazy { ContextCompat.getMainExecutor(this) }


    override val layoutResId: Int
        get() = R.layout.signup_activity

    override val snackbarContainerView: View
        get() = signupContainer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.signup_activity_title)

        this.signupContainer = findViewById(R.id.signup_container)
        this.enterEmailLayout = findViewById(R.id.signup_enter_email_layout)
        this.enterPasswordLayout = findViewById(R.id.signup_enter_password_layout)
        this.confirmPasswordLayout = findViewById(R.id.signup_confirm_password_layout)
        this.signupButton = findViewById(R.id.sign_up_button)
        this.progressBar = findViewById(R.id.progress_bar)
        signupButton.setOnClickListener {
            val email = enterEmailLayout.editText?.text.toString()
            val password = enterPasswordLayout.editText?.text.toString()
            val confirmedPass = confirmPasswordLayout.editText?.text.toString()
            presenter.signUpUser(email, password, confirmedPass)
        }
    }

    override fun displayEmailError(error: Int) {
        enterEmailLayout.error = getString(error)
        enterEmailLayout.isErrorEnabled = true
    }

    override fun displayPasswordError(error: Int) {
        enterPasswordLayout.error = getString(error)
        enterPasswordLayout.isErrorEnabled = true
        confirmPasswordLayout.isErrorEnabled = true
    }

    override fun displayConfirmedPasswordError(error: Int) {
        confirmPasswordLayout.error = getString(error)
        confirmPasswordLayout.isErrorEnabled = true
    }

    override fun disableSignUpButton() {
        super.disableSignUpButton()
        signupButton.isEnabled = false
    }

    override fun enableSignUpButton() {
        super.enableSignUpButton()
        signupButton.isEnabled = true
    }

    override fun displaySnackbar(message: Int, length: Int, messageType: SnackbarMessage.Type) {

        val snackbarMessage = when(messageType) {
            SnackbarMessage.Type.PRIORITY -> SnackbarMessage.Priority(R.string.signup_go_to_login_action) { view ->
                Timber.i("@@@ click detected on snackbar action item!")
                finish()
            }

            else -> SnackbarMessage.getFromType(messageType)
        }

        displaySnackbarMessage(message, length, snackbarMessage)
    }

    override fun showProgressBar() {
        progressBar.toVisible()
    }

    override fun hideProgressBar() {
        progressBar.toGone()
    }

    override fun launchNextActivity() {
        // TODO: time to launch next activity now!!
        launch(AccountSetupActivity::class.java)
    }

    override fun showBiometricPrompt(callback: BiometricPrompt.AuthenticationCallback,
                                     cryptoObject: BiometricPrompt.CryptoObject) {

        BiometricPrompt(this, executor, callback)
            .authenticate(BiometricPrompt.PromptInfo.Builder()
                .setTitle(getString(R.string.signup_biometric_prompt_title))
                .setDescription(getString(R.string.signup_biometric_prompt_message))
                .setNegativeButtonText(getString(R.string.signup_biometric_negative_button_text))
                .build(),
                cryptoObject)

    }

    override val activityContext: Context
        get() = this
}