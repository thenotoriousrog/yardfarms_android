package com.appme.yardfarms.authentication.biometrics

import android.security.keystore.KeyProperties
import androidx.biometric.BiometricPrompt
import com.appme.yardfarms.backend.AuthenticationUtils
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.SecretKey

abstract class Authenticator : BiometricPrompt.AuthenticationCallback() {

    companion object {
        protected const val ANDROID_KEYSTORE_ALIAS = "AndroidKeyStore"
    }

    protected val secretKey: SecretKey

    protected val cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                + KeyProperties.BLOCK_MODE_CBC + "/"
                + KeyProperties.ENCRYPTION_PADDING_PKCS7)

    init {
        val keystore = KeyStore.getInstance(ANDROID_KEYSTORE_ALIAS)
        keystore.load(null)
        secretKey =  keystore.getKey(ANDROID_KEYSTORE_ALIAS, null) as? SecretKey ?: AuthenticationUtils.generateSecretKey(ANDROID_KEYSTORE_ALIAS)
    }
}