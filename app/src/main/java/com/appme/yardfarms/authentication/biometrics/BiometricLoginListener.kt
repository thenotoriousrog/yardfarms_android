package com.appme.yardfarms.authentication.biometrics

/**
 * Handles actions when the user is logging in
 */
interface BiometricLoginListener {

    /**
     * Called when the user's information was encrypted correctly.
     * Important because it's will help determine the app behavior going forward
     */
    fun onEncryptionSuccess()

    /**
     * Report that the user's information was not successfully encrypted.
     */
    fun onEncryptionError(failure: AuthFailure)

    /**
     * Returned when the user's credentials are correctly decrypted and ready to be used.
     * @param decryptedEmail - the user's valid email
     * @param decryptedPassword - the user's valid password
     */
    fun onDecryptionSuccess(decryptedEmail: String, decryptedPassword: String)

    /**
     * Occurs when an error occurs decrypting the user's information.
     * This can be overridden if the user manually enter's their credentials.
     */
    fun onDecryptionError(failure: AuthFailure)

    /**
     * Something has caused the user to not be logged in using biometric's. This error can be fired by
     * LoginAuthenticator or by Biometric api.
     * User will have to log in manually.
     * @param failure - the type of failure that is returned.
     * @param errString - an error string that is being returned. This is often returned when triggered by an error called
     * by the Biometric api. Null is often passed if triggered by LoginAuthenticator but not gauranteed.
     */
    fun onAuthFailure(failure: AuthFailure, errString: CharSequence? = null)

}