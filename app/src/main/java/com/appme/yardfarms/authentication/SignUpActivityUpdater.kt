package com.appme.yardfarms.authentication

import android.content.Context
import androidx.annotation.StringRes
import androidx.biometric.BiometricPrompt
import com.appme.yardfarms.SnackbarMessage

interface SignUpActivityUpdater {

    fun displayEmailError(@StringRes error: Int)

    fun displayPasswordError(@StringRes error: Int)

    fun displayConfirmedPasswordError(@StringRes error: Int)

    fun disableSignUpButton() {
        showProgressBar()
    }

    fun enableSignUpButton() {
        hideProgressBar()
    }

    fun displaySnackbar(@StringRes message: Int, length: Int, messageType: SnackbarMessage.Type)

    fun showProgressBar()

    fun hideProgressBar()

    fun launchNextActivity()

    /**
     * Triggers the UI to display a biometric prompt
     * @param callback - the callback for the dialog to make responses to
     * @param cryptoObject - used for biometric validation
     */
    fun showBiometricPrompt(callback: BiometricPrompt.AuthenticationCallback,
                            cryptoObject: BiometricPrompt.CryptoObject)

    /**
     * Retrieves the activity's current context.
     * **Note: Be careful when using this in the presenter. It's usage should be minimum
     */
    val activityContext: Context

}