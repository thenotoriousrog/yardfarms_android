package com.appme.yardfarms.authentication

import androidx.annotation.StringRes
import com.appme.yardfarms.SnackbarMessage

interface ForgotPasswordActivityUpdater {


    /**
     * Displays an error under the email textbox
     * @param error - the string res error message
     */
    fun displayEmailError(@StringRes error: Int)


    fun disableSendRecoveryEmailButton()

    fun enableSendRecoveryEmailButton()

    fun showProgressBar()

    fun hideProgressBar()

    fun displaySnackbar(@StringRes message: Int, length: Int, messageType: SnackbarMessage.Type)


    /**
     * Finishes the current activity
     */
    fun terminate()
}