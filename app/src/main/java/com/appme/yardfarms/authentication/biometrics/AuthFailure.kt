package com.appme.yardfarms.authentication.biometrics

/**
 * Various different reasons when using biometrics
 */
enum class AuthFailure {
    GENERAL_FAILURE, // Applies to any other type of failure not listed here
    INVALID_CREDENTIALS, // applies if the credentials passed in are invalid
    DIRTY_SENSOR, // systems detects or suspects a dirty sensor
    SCAN_TOO_FAST, // user moved too fast
    SCAN_TOO_SLOW, // user moved too slow or not at all
    LOCKOUT, // biometric api has locked the user out
    TIMEOUT, // biometric api has timed out.
    USE_PASSWORD_REQUESTED // User has opted to use their password instead of fingerprint login


}