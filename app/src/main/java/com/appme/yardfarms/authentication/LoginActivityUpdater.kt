package com.appme.yardfarms.authentication

import android.content.Context
import androidx.annotation.StringRes
import androidx.biometric.BiometricPrompt
import com.appme.yardfarms.SnackbarMessage

interface LoginActivityUpdater {

    fun disableViews() {
        showProgressBar()
    }

    fun enableViews() {
        hideProgressBar()
    }

    fun displayEmailError(@StringRes error: Int)

    fun displayPasswordError(@StringRes error: Int)

    fun displaySnackbar(@StringRes message: Int, length: Int, messageType: SnackbarMessage.Type)

    fun showProgressBar()

    fun hideProgressBar()

    /**
     * Can be either the HomeActivity or the AccountSetupActivity depending on if the user has
     * stored that information or not yet
     */
    fun launchNextActivity()

    /**
     * Causes the UI to show the biometric prompt
     * @param isRegisteringBiometrics - true if registering, false if already registered
     * @param callback - the callback to report user actions to
     * @param cryptoObject - the crypto object to use for biometric validation
     */
    fun showBiometricPrompt(isRegisteringBiometrics: Boolean,
                            callback: BiometricPrompt.AuthenticationCallback,
                            cryptoObject: BiometricPrompt.CryptoObject)

    /**
     * Forces the biometric prompt to close
     */
    fun cancelBiometricPrompt()

    /**
     * Retrieves the activity's current context.
     * **Note: Be careful when using this in the presenter. It's usage should be minimum
     */
    val activityContext: Context
}