package com.appme.yardfarms.authentication

import android.content.Intent
import android.content.SharedPreferences
import androidx.biometric.BiometricManager
import com.appme.yardfarms.R
import com.appme.yardfarms.SnackbarMessage
import com.appme.yardfarms.authentication.biometrics.AuthFailure
import com.appme.yardfarms.authentication.biometrics.BiometricLoginListener
import com.appme.yardfarms.authentication.biometrics.LoginAuthenticator
import com.appme.yardfarms.backend.Constants
import com.appme.yardfarms.backend.persistance.FirebaseHelper
import com.appme.yardfarms.backend.persistance.listeners.FirebaseCallback
import com.appme.yardfarms.util.edit
import com.appme.yardfarms.util.isValidEmail
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.DatabaseError
import timber.log.Timber

class LoginActivityPresenter(private val updater: LoginActivityUpdater,
                             private val prefs: SharedPreferences) : BiometricLoginListener {


    private lateinit var userEmail: String
    private lateinit var userPassword: String
    private val loginAuthenticator by lazy { LoginAuthenticator(this, prefs) }
    private val biometricManager by lazy { BiometricManager.from(updater.activityContext) }
    private val hasRegisteredBiometrics = prefs.getBoolean(Constants.PREFS_HAS_REGISTERED_BIOMETRICS, false)
    private val hasCreatedAccount = prefs.getBoolean(Constants.PREFS_HAS_CREATED_ACCOUNT, false)


    private fun handleError(exception: Exception?) {
        when(exception) {
            is FirebaseAuthInvalidCredentialsException -> {
                updater.displayEmailError(R.string.login_invalid_credentials)
                updater.displayPasswordError(R.string.login_invalid_credentials)
            }

            is FirebaseAuthWeakPasswordException -> updater.displayPasswordError(R.string.signup_error_password_too_weak)
            is FirebaseAuthInvalidUserException -> updater.displaySnackbar(R.string.login_invalid_user, Snackbar.LENGTH_LONG, SnackbarMessage.Type.ERROR)
            else -> updater.displaySnackbar(R.string.login_error_fatal, Snackbar.LENGTH_LONG, SnackbarMessage.Type.ERROR)
        }
    }

    /**
     * Handles automatically logging in the user to the app.
     */
    fun attemptAutoLogin() {

        if(FirebaseHelper.isLoggedIn) {
            updater.launchNextActivity()
        } else {

            if(hasCreatedAccount && hasRegisteredBiometrics) {

                when(biometricManager.canAuthenticate()) {
                    BiometricManager.BIOMETRIC_SUCCESS -> showBiometricLogin(false)
                    BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> Unit // do nothing
                    BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> Unit // do nothing
                    BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> updater.displaySnackbar(R.string.login_biometric_hardware_unavailable, Snackbar.LENGTH_SHORT, SnackbarMessage.Type.SECONDARY)
                }

            }
        }
    }

    /**
     * Tells the presenter that the user has clicked the biometric button
     */
    fun biometricButtonClicked() {
        showBiometricLogin(false)
    }

    /**
     * True if biometrics is available to be used, false if not
     */
    val isBiometricsAvailable: Boolean
        get() {
            if(hasCreatedAccount && hasRegisteredBiometrics) {
                return when(biometricManager.canAuthenticate()) {
                    BiometricManager.BIOMETRIC_SUCCESS -> true
                    else -> false
                }
            }

            return false
        }

    private fun showBiometricLogin(isRegisteringBiometrics: Boolean) {
        val cryptoObject = loginAuthenticator.cryptoObject
        if(cryptoObject == null) {
            Timber.w("Unable to show biometric prompts because cryptoObject is null!")
            updater.displaySnackbar(R.string.login_biometric_prompt_error, Snackbar.LENGTH_SHORT, SnackbarMessage.Type.PRIORITY)
            return
        }

        updater.showBiometricPrompt(isRegisteringBiometrics, loginAuthenticator, cryptoObject)
    }

    fun attemptLoginWithGoogle(data: Intent?) {
        val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
        try {
            val account = task.getResult(ApiException::class.java)
            val credential = GoogleAuthProvider.getCredential(account?.idToken, null)

            FirebaseHelper.loginUserWithGoogle(credential, object : FirebaseCallback {
                override fun onCancel() {
                    updater.enableViews()
                }

                override fun onError(exception: Exception?) {
                    updater.displaySnackbar(R.string.login_with_google_error, Snackbar.LENGTH_LONG, SnackbarMessage.Type.ERROR)
                }

                override fun onDatabaseError(databaseError: DatabaseError) { } // not needed

                override fun onSuccess(item: Any?) {
                    updater.launchNextActivity()
                }
            })

        } catch (ex: ApiException) {
            Timber.e(ex,"Caught exception while trying to access user's google account! Status code == ${ex.statusCode}")
            updater.enableViews()
            updater.displaySnackbar(R.string.login_with_google_auth_cred_errors, Snackbar.LENGTH_LONG, SnackbarMessage.Type.ERROR)
        }
    }

    /**
     * Handles verifying information and logging the user into the app.
     */
    fun loginButtonClicked(email: String?, password: String?) {

        updater.disableViews()

        var isValid = true
        if(email.isNullOrBlank() || !email.isValidEmail()) {
            updater.displayEmailError(R.string.enter_email_error)
            updater.enableViews()
            isValid = false
        }

        if(password.isNullOrBlank()) {
            updater.displayPasswordError(R.string.enter_password_error)
            updater.enableViews()
            isValid = false
        }

        if(!isValid) return

        // these will never be null despite the compiler warning
        this.userEmail = email!!
        this.userPassword = password!!

        if(!hasRegisteredBiometrics) { // we want to request biometrics before logging in the user!
            loginAuthenticator.isEncrypting = true
            loginAuthenticator.email = email
            loginAuthenticator.password = password
            showBiometricLogin(true)
        } else {
            logInUser(email, password)
        }

    }

    private fun logInUser(email: String, password: String) {

        FirebaseHelper.loginUser(email, password, object : FirebaseCallback {

            override fun onCancel() {
                updater.enableViews()
            }

            override fun onError(exception: Exception?) {
                handleError(exception)
                updater.enableViews()
            }

            override fun onDatabaseError(databaseError: DatabaseError) { } // not needed

            override fun onSuccess(item: Any?) {
                updater.hideProgressBar()

                prefs.edit {
                    putBoolean(Constants.PREFS_HAS_CREATED_ACCOUNT, true)
                }

                updater.launchNextActivity()
            }
        })
    }

    override fun onEncryptionSuccess() {
        logInUser(userEmail, userPassword)
    }

    override fun onEncryptionError(failure: AuthFailure) {
        updater.cancelBiometricPrompt()
        if(failure == AuthFailure.INVALID_CREDENTIALS) {
            updater.displaySnackbar(R.string.login_biometric_prompt_invalid_credentials_error, Snackbar.LENGTH_LONG, SnackbarMessage.Type.ERROR)
        } else {
            updater.displaySnackbar(R.string.login_biometric_prompt_encryption_error, Snackbar.LENGTH_LONG, SnackbarMessage.Type.ERROR)
        }
    }

    override fun onDecryptionSuccess(decryptedEmail: String, decryptedPassword: String) {
        this.userEmail = decryptedEmail
        this.userPassword = decryptedPassword
        logInUser(decryptedEmail, decryptedPassword)
    }

    override fun onDecryptionError(failure: AuthFailure) {
       Timber.e("User's credentials could not be decrypted!")

        prefs.edit {
            putBoolean(Constants.PREFS_HAS_REGISTERED_BIOMETRICS, false)
        }

        updater.cancelBiometricPrompt()
        updater.displaySnackbar(R.string.login_biometric_prompt_decryption_error, Snackbar.LENGTH_LONG, SnackbarMessage.Type.ERROR)
    }

    override fun onAuthFailure(failure: AuthFailure, errString: CharSequence?) {
        Timber.w("Biometrics encountered a failure when authenticating -- ${failure.name}")
    }

}