package com.appme.yardfarms.authentication

import android.content.SharedPreferences
import androidx.biometric.BiometricManager
import com.appme.yardfarms.R
import com.appme.yardfarms.SnackbarMessage
import com.appme.yardfarms.authentication.biometrics.AuthFailure
import com.appme.yardfarms.authentication.biometrics.BiometricSignUpListener
import com.appme.yardfarms.authentication.biometrics.SignUpAuthenticator
import com.appme.yardfarms.backend.Constants
import com.appme.yardfarms.backend.persistance.FirebaseHelper
import com.appme.yardfarms.backend.persistance.listeners.FirebaseCallback
import com.appme.yardfarms.util.edit
import com.appme.yardfarms.util.isValidEmail
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.google.firebase.database.DatabaseError
import timber.log.Timber

class SignUpActivityPresenter(private val updater: SignUpActivityUpdater,
                              private val prefs: SharedPreferences) : BiometricSignUpListener {

    private val signUpAuthenticator by lazy { SignUpAuthenticator(this, prefs) }
    private val biometricManager by lazy { BiometricManager.from(updater.activityContext) }


    private fun fieldsAreValid(email: String?,
                               password: String?,
                               confirmedPass: String?): Boolean {

        var isValid = true
        if(email.isNullOrBlank() || !email.isValidEmail()) {
            updater.displayEmailError(R.string.enter_email_error)
            updater.enableSignUpButton()
            isValid = false
        }

        if(password.isNullOrBlank()) {
            updater.displayPasswordError(R.string.enter_password_error)
            updater.enableSignUpButton()
            isValid = false
        }

        if(confirmedPass.isNullOrBlank()) {
            updater.displayConfirmedPasswordError(R.string.enter_password_error)
            updater.enableSignUpButton()
            isValid = false
        }

        if(password != confirmedPass) {
            updater.displayPasswordError(R.string.signup_passwords_do_not_match)
            updater.displayConfirmedPasswordError(R.string.signup_passwords_do_not_match)
            updater.enableSignUpButton()
            isValid = false
        }

        return isValid
    }

    fun handleErrors(exception: Exception?) {
        when(exception) {
            is FirebaseAuthWeakPasswordException -> {
                updater.displayPasswordError(R.string.signup_error_password_too_weak)
                updater.displayConfirmedPasswordError(R.string.signup_error_password_too_weak)
            }

            is FirebaseAuthUserCollisionException -> updater.displaySnackbar(R.string.sign_up_account_already_exists, Snackbar.LENGTH_INDEFINITE, SnackbarMessage.Type.PRIORITY)

            else -> updater.displaySnackbar(R.string.signup_error_fatal, Snackbar.LENGTH_LONG, SnackbarMessage.Type.ERROR)
        }
    }

    private fun handleBiometricRegistration() {
        when(biometricManager.canAuthenticate()) {
            BiometricManager.BIOMETRIC_SUCCESS -> {
                val cryptoObject = signUpAuthenticator.cryptoObject
                if(cryptoObject == null) {
                    Timber.w("Unable to show biometric prompt because CryptoObject is null!")
                    updater.launchNextActivity()
                    return
                }

                updater.showBiometricPrompt(signUpAuthenticator, cryptoObject)
            }
            else -> Unit // do nothing
        }
    }

    /**
     * Verifies information and signs up the user for our application.
     */
    fun signUpUser(email: String?,
                   password: String?,
                   confirmedPass: String?) {

        if(fieldsAreValid(email, password, confirmedPass)) {

            updater.disableSignUpButton()

            FirebaseHelper.signupUser(email!!, password!!, object : FirebaseCallback {

                override fun onCancel() {
                    updater.enableSignUpButton()
                }

                override fun onError(exception: Exception?) {
                    handleErrors(exception)
                    updater.enableSignUpButton()
                }

                override fun onDatabaseError(databaseError: DatabaseError) { } // not needed

                override fun onSuccess(item: Any?) {
                    updater.hideProgressBar()

                    prefs.edit {
                        putBoolean(Constants.PREFS_HAS_CREATED_ACCOUNT, true)
                    }

                    signUpAuthenticator.email = email
                    signUpAuthenticator.password = password
                    handleBiometricRegistration()
                }
            })
        }

    }

    override fun onEncryptionSuccess() {
        updater.launchNextActivity()
    }

    override fun onAuthFailure(failure: AuthFailure, errString: CharSequence?) {
        updater.launchNextActivity()
        Timber.w("Biometric authentication failure -- ${failure.name}")
    }
}