package com.appme.yardfarms.authentication

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.view.View
import android.widget.ImageButton
import android.widget.ProgressBar
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.appme.yardfarms.BaseActivity
import com.appme.yardfarms.HomeActivity
import com.appme.yardfarms.R
import com.appme.yardfarms.SnackbarMessage
import com.appme.yardfarms.accountsetup.AccountSetupActivity
import com.appme.yardfarms.backend.AuthenticationUtils
import com.appme.yardfarms.backend.Constants
import com.appme.yardfarms.util.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import timber.log.Timber
import java.nio.charset.Charset
import javax.crypto.Cipher

class LoginActivity : BaseActivity(), LoginActivityUpdater {

    private lateinit var loginActivityContainer: ConstraintLayout
    private lateinit var loginEmailLayout: TextInputLayout
    private lateinit var loginPasswordLayout: TextInputLayout
    private lateinit var forgotPasswordButton: MaterialButton
    private lateinit var loginButton: MaterialButton
    private lateinit var loginWithGoogleButton: MaterialButton
    private lateinit var signUpButton: MaterialButton
    private lateinit var biometricButton: ImageButton
    private lateinit var progressBar: ProgressBar
    private val presenter by lazy {   LoginActivityPresenter(this, generalSharedPrefs) }
    private val excecutor by lazy { ContextCompat.getMainExecutor(this) }
    private lateinit var biometricPrompt: BiometricPrompt

    private lateinit var googleSignInOptions: GoogleSignInOptions
    private lateinit var googleSignInClient: GoogleSignInClient

    override val layoutResId: Int
        get() = R.layout.login_activity

    override val snackbarContainerView: View
        get() = loginActivityContainer

    companion object {
        private const val GOOGLE_SIGN_IN_RES_CODE = 1000 // Response code for google sign in task.
    }

    private fun prepareGoogleSignInConfig() {
        this.googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.google_sign_in_web_client_id))
            .requestEmail()
            .build()

        this.googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)
    }

    private fun allocateViews() {
        this.loginActivityContainer = findViewById(R.id.login_activity_container)
        this.loginEmailLayout = findViewById(R.id.login_enter_email_layout)
        this.loginPasswordLayout = findViewById(R.id.login_enter_password_layout)
        this.forgotPasswordButton = findViewById(R.id.forgot_password_button)
        this.loginButton = findViewById(R.id.login_button)
        this.loginWithGoogleButton = findViewById(R.id.login_with_google)
        this.signUpButton = findViewById(R.id.sign_up_button)
        this.biometricButton = findViewById(R.id.biometric_button)
        this.progressBar = findViewById(R.id.progress_bar)
    }

    private fun generatePromptInfo(isRegisteringBiometrics: Boolean): BiometricPrompt.PromptInfo {

        return if(!isRegisteringBiometrics) {
            BiometricPrompt.PromptInfo.Builder()
                .setTitle(getString(R.string.login_biometric_prompt_title))
                .setDescription(getString(R.string.login_biometric_prompt_message))
                .setNegativeButtonText(getString(R.string.login_biometric_prompt_negative_button_text))
                .build()
        } else {
            BiometricPrompt.PromptInfo.Builder()
                .setTitle(getString(R.string.login_biometric_prompt_title_register))
                .setDescription(getString(R.string.login_biometric_prompt_message_register))
                .setNegativeButtonText(getString(R.string.login_biometric_prompt_negative_button_text_register))
                .build()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        prepareGoogleSignInConfig()

        supportActionBar?.title = getString(R.string.login_activity_title)
        allocateViews()

        loginButton.setOnClickListener {
            clearErrors()
            val email = loginEmailLayout.editText?.text.toString()
            val password = loginPasswordLayout.editText?.text.toString()
            presenter.loginButtonClicked(email, password)
        }

        signUpButton.setOnClickListener {
            launch(SignUpActivity::class.java)
        }

        loginWithGoogleButton.setOnClickListener {
            val signInWithGoogleIntent = googleSignInClient.signInIntent
            startActivityForResult(signInWithGoogleIntent, GOOGLE_SIGN_IN_RES_CODE)
        }

        forgotPasswordButton.setOnClickListener {
            launch(ForgotPasswordActivity::class.java)
        }

        biometricButton.setOnClickListener {
            presenter.biometricButtonClicked()
        }

        if(presenter.isBiometricsAvailable) {
            biometricButton.toVisible()
        } else {
            biometricButton.toInvisible()
        }

        presenter.attemptAutoLogin()

    }

    private fun clearErrors() {
        loginEmailLayout.isErrorEnabled = false
        loginPasswordLayout.isErrorEnabled = false
    }

    override fun disableViews() {
        super.disableViews()
        loginButton.isEnabled = false
        forgotPasswordButton.isEnabled = false
        signUpButton.isEnabled = false
    }

    override fun enableViews() {
        super.enableViews()
        loginButton.isEnabled = true
        forgotPasswordButton.isEnabled = true
        signUpButton.isEnabled = true
    }

    override fun displayEmailError(error: Int) {
        loginEmailLayout.error = getString(error)
        loginEmailLayout.isErrorEnabled = true
    }

    override fun displayPasswordError(error: Int) {
        loginPasswordLayout.error = getString(error)
        loginPasswordLayout.isErrorEnabled = true
    }

    override fun displaySnackbar(message: Int, length: Int, messageType: SnackbarMessage.Type) {
        displaySnackbarMessage(message, length, SnackbarMessage.getFromType(messageType))
    }

    override fun hideProgressBar() {
        progressBar.toGone()
    }

    override fun showProgressBar() {
        progressBar.toVisible()
    }

    override fun launchNextActivity() {
        val hasFinishedAccountSetup = generalSharedPrefs.getBoolean(Constants.PREFS_HAS_FINISHED_ACCOUNT_SETUP, false)
        if(hasFinishedAccountSetup) {
            startActivity(HomeActivity.getNewIntent(this))
        } else {
            launch(AccountSetupActivity::class.java)
        }
        finish()
    }

    override fun showBiometricPrompt(isRegisteringBiometrics: Boolean,
                                     callback: BiometricPrompt.AuthenticationCallback,
                                     cryptoObject: BiometricPrompt.CryptoObject) {

        // make this into one prompt!!!
        biometricPrompt = BiometricPrompt(this, excecutor, callback)
        biometricPrompt.authenticate(generatePromptInfo(isRegisteringBiometrics), cryptoObject)
    }

    override fun cancelBiometricPrompt() {
        biometricPrompt.cancelAuthentication()
    }

    override val activityContext: Context
        get() = this

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == GOOGLE_SIGN_IN_RES_CODE) {
            presenter.attemptLoginWithGoogle(data)
        }
    }
}