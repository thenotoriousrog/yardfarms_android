package com.appme.yardfarms.authentication.biometrics

import android.content.SharedPreferences
import android.util.Base64
import androidx.biometric.BiometricPrompt
import com.appme.yardfarms.backend.Constants
import com.appme.yardfarms.util.edit
import timber.log.Timber
import java.nio.charset.Charset
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec

/**
 * Handles enrolling the user into biometrics
 * @param listener - listens to authentication callbacks and fires response to user
 * @param sharedPrefs - the preferences we want to use for storing user information
 */
class SignUpAuthenticator(private val listener: BiometricSignUpListener,
                          private val sharedPrefs: SharedPreferences) : Authenticator() {

    private var cipherIsInitialized = false // enforced one cipher initialization per authenticator

    /**
     * The user's current unencrypted email
     */
    lateinit var email: String

    /**
     * The user's current unencrypted password
     */
    lateinit var password: String

    /**
     * Initializes cipher for this authenticator
     */
    private fun initCipher(): Boolean {
        return try {
            val iv: ByteArray
            cipher.init(Cipher.ENCRYPT_MODE, secretKey)
            val ivParams: IvParameterSpec = cipher.parameters.getParameterSpec(IvParameterSpec::class.java)
            iv = ivParams.iv
            val encodedIv = String(Base64.encode(iv, Base64.NO_WRAP))

            sharedPrefs.edit { putString(Constants.PREFS_IV_ENCODED, encodedIv) }

            cipherIsInitialized = true
            true
        } catch (ex: Exception) { // **Note: There's many exceptions that can be thrown here. For now, using the general exception class is the cleanest solution.
            Timber.e(ex, "Caught exception when initializing cipher!")
            false
        }
    }

    /**
     * CryptoObject used for biometrics.
     * Null if object cannot be generated due to something going wrong.
     */
    val cryptoObject: BiometricPrompt.CryptoObject?
        get() {
            return if(initCipher()) {
                BiometricPrompt.CryptoObject(cipher)
            } else {
                Timber.e("Unable to properly generate CryptoObject")
                null
            }
        }

    private fun encryptData(result: BiometricPrompt.AuthenticationResult) {
        val dataToEncrypt = "$email:$password"
        val encryptedData = result.cryptoObject?.cipher?.doFinal(dataToEncrypt.toByteArray(Charset.defaultCharset()))
        val encoded = Base64.encodeToString(encryptedData, Base64.NO_WRAP)

        sharedPrefs.edit {
            putString(Constants.PREFS_USER_CREDENTIALS, encoded)
            putBoolean(Constants.PREFS_HAS_REGISTERED_BIOMETRICS, true)
        }

        listener.onEncryptionSuccess()
    }

    override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
        encryptData(result)
    }

    override fun onAuthenticationFailed() {
        super.onAuthenticationFailed()
        Timber.w("Biometric api is reporting an authentication failure!")
        listener.onAuthFailure(AuthFailure.GENERAL_FAILURE)
    }

    override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {

        when (errorCode) {
            BiometricPrompt.ERROR_LOCKOUT or BiometricPrompt.ERROR_LOCKOUT_PERMANENT -> listener.onAuthFailure(AuthFailure.LOCKOUT)
            BiometricPrompt.ERROR_NEGATIVE_BUTTON -> listener.onAuthFailure(AuthFailure.USE_PASSWORD_REQUESTED)

            else -> {
                Timber.w("Authentication has errored out with errorCode = $errorCode and errString = $errString")
                listener.onAuthFailure(AuthFailure.GENERAL_FAILURE)
            }
        }
    }

}