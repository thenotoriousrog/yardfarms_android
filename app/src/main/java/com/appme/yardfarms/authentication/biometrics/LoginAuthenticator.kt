package com.appme.yardfarms.authentication.biometrics

import android.content.SharedPreferences
import android.security.keystore.KeyProperties
import android.util.Base64
import androidx.biometric.BiometricPrompt
import com.appme.yardfarms.backend.AuthenticationUtils
import com.appme.yardfarms.backend.Constants
import com.appme.yardfarms.util.edit
import timber.log.Timber
import java.nio.charset.Charset
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec

/**
 * Handles Biometric actions in regards to logging for the user
 * @param listener - listener to fireback callback actions to the calling class
 * @param sharedPrefs - the preferences we want to use for storing user information
 */
class LoginAuthenticator(private val listener: BiometricLoginListener,
                         private val sharedPrefs: SharedPreferences) : Authenticator() {

    private var cipherIsInitialized = false // enforced one cipher initialization per authenticator

    /**
     * This flag determines whether or not we want to encrypt data (when registering) or
     * decrypt data (when logging in).
     */
    var isEncrypting = false

    /**
     * The user's current unencrypted email
     */
    var email: String? = null

    /**
     * The user's current unencrypted password
     */
    var password: String? = null

    /**
     * Initializes the cipher for this Authenticator
     * @return returns true if initialization was successful, false if not.
     */
    private fun initCipher(mode: Int): Boolean {
        try {
            val iv: ByteArray
            val ivParams: IvParameterSpec

            if(mode == Cipher.ENCRYPT_MODE) {
                cipher.init(mode, secretKey)
                ivParams = cipher.parameters.getParameterSpec(IvParameterSpec::class.java)
                iv = ivParams.iv
                val encodedIv = String(Base64.encode(iv, Base64.NO_WRAP))

                sharedPrefs.edit { putString(Constants.PREFS_IV_ENCODED, encodedIv) }

            } else {
                val encodedIv = sharedPrefs.getString(Constants.PREFS_IV_ENCODED, null)
                val decoded = Base64.decode(encodedIv, Base64.NO_WRAP)
                iv = decoded
                ivParams = IvParameterSpec(iv)
                cipher.init(mode, secretKey, ivParams)
            }

            cipherIsInitialized = true
            return true
        } catch (ex: Exception) { // **Note: There are many different exceptions that can be thrown here. Because of this catching the generic Exception object is the best choice until Kotlin has multi-catch.
            Timber.e(ex,"Caught exception when initializing cipher!")
            // TODO: fire failed callback to user!

            return false
        }
    }

    /**
     * CryptoObject used for Biometrics.
     * null if something went wrong and unable to use the valid
     */
    val cryptoObject: BiometricPrompt.CryptoObject?
        get() {
            return if(isEncrypting && initCipher(Cipher.ENCRYPT_MODE)) {
                BiometricPrompt.CryptoObject(cipher)
            } else if (!isEncrypting && initCipher(Cipher.DECRYPT_MODE)) {
                BiometricPrompt.CryptoObject(cipher)
            } else {
                Timber.e("Unable to properly generate CryptoObject!")
                null
            }
        }

    private fun encryptData(result: BiometricPrompt.AuthenticationResult) {
        if(email.isNullOrBlank() || password.isNullOrBlank()) {
            listener.onEncryptionError(AuthFailure.INVALID_CREDENTIALS)
            return
        }

        val dataToEncrypt = "$email:$password"
        val encryptedData = result.cryptoObject?.cipher?.doFinal(dataToEncrypt.toByteArray(Charset.defaultCharset()))
        val encoded = Base64.encodeToString(encryptedData, Base64.NO_WRAP)

        sharedPrefs.edit {
            putString(Constants.PREFS_USER_CREDENTIALS, encoded)
            putBoolean(Constants.PREFS_HAS_REGISTERED_BIOMETRICS, true)
        }

        listener.onEncryptionSuccess()
    }

    private fun decryptData(result: BiometricPrompt.AuthenticationResult) {
        val encryptedUserCreds = sharedPrefs.getString(Constants.PREFS_USER_CREDENTIALS, null)
        if(!encryptedUserCreds.isNullOrBlank()) {
            val bytes = Base64.decode(encryptedUserCreds, Base64.NO_WRAP)
            val convertedString = result.cryptoObject?.cipher?.doFinal(bytes)

            if(convertedString == null) {
                Timber.e("The returned cipher is null!! User cannot be logged in with their fingerprint!")
                listener.onDecryptionError(AuthFailure.INVALID_CREDENTIALS)

            } else {
                val decoded = String(convertedString)
                val credsSplit = decoded.split(":")

                listener.onDecryptionSuccess(decryptedEmail = credsSplit[0], decryptedPassword = credsSplit[1])
            }

        } else {
            Timber.e("The user does not have any credentials encrypted that we can use!")
            listener.onDecryptionError(AuthFailure.INVALID_CREDENTIALS)
        }
    }

    override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
        if(isEncrypting) {
            encryptData(result)
        } else {
            decryptData(result)
        }
    }

    override fun onAuthenticationFailed() {
        super.onAuthenticationFailed()
        Timber.w("Biometric api is reporting an authentication failure!")
        listener.onAuthFailure(AuthFailure.GENERAL_FAILURE)
    }

    override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {

        when (errorCode) {
            BiometricPrompt.ERROR_LOCKOUT or BiometricPrompt.ERROR_LOCKOUT_PERMANENT -> listener.onAuthFailure(AuthFailure.LOCKOUT)
            BiometricPrompt.ERROR_NEGATIVE_BUTTON -> listener.onAuthFailure(AuthFailure.USE_PASSWORD_REQUESTED)

            else -> {
                Timber.w("Authentication has errored out with errorCode = $errorCode and errString = $errString")
                listener.onAuthFailure(AuthFailure.GENERAL_FAILURE)
            }
        }
    }

}