package com.appme.yardfarms.backend.persistance.listeners

interface FirebaseItemRetriever<T> {

    /**
     * Called when the item has been retrieved from Firebase
     * @param item - the item retrieved **Note: can be null despite a successful retrieval
     */
    fun onItemRetrieved(item: T?)

    /**
     * Called whenever an item retrieval has failed
     */
    fun onError()

}