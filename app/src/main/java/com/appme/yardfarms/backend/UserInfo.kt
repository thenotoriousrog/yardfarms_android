package com.appme.yardfarms.backend

/**
 * Represents a user's created profile
 * @param userId - the user's unique firebase id
 * @param name - the name of this
 * @param age - the age of this user
 * @param email - the email of this user
 * @param bio - user's bio
 * @param profilePicture - url to the user's profile picture
 * @param address - the user's entered address
 */
data class UserInfo(val userId: String?,
                    val name: String,
                    var age: String,
                    var email: String?,
                    var bio: String?,
                    var profilePicture: String?,
                    var address: Address?) {

    constructor() : this(null, "", "", null, null, null, null)

    override fun equals(other: Any?): Boolean {
        if(other == null) return false
        if(other === this) return true
        if(other !is UserInfo) return false

        val otherUserInfo: UserInfo = other

        return this.name == otherUserInfo.name
                && this.age == otherUserInfo.age
                && this.email == otherUserInfo.email
                && this.bio == otherUserInfo.bio
                && this.profilePicture == otherUserInfo.profilePicture
                && this.address == otherUserInfo.address
    }

    override fun hashCode(): Int {
        var result = 17
        result = 31 * result + name.hashCode()
        result = 31 * result + age.hashCode()
        result = 31 * result + email.hashCode()
        result = 31 * result + bio.hashCode()
        return result
    }
}