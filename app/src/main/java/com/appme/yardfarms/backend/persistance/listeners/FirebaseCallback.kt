package com.appme.yardfarms.backend.persistance.listeners

import androidx.annotation.StringRes
import com.google.firebase.database.DatabaseError

/**
 * General listener which will helps with bridging the gap between firebase errors and our own custom
 * error handling.
 */
interface FirebaseCallback {

    /**
     * Firebase reports that an action was cancelled
     */
    fun onCancel()

    /**
     * Reports that the operation that Firebase was attempting to do has failed completely. This kind of error
     * is more general and may or may not be specific to database specific actions.
     * @param exception - an optional exception for advanced error handling in the calling activity.
     */
    fun onError(exception: Exception? = null)

    /**
     * Reports an error that is related to a database action such as adding or updating fields.
     * @param databaseError - the error returned by firebase
     */
    fun onDatabaseError(databaseError: DatabaseError)

    /**
     * Reports that the firebase action succeeded
     * @param item - a returnable item of any type depending on what needs to be returned
     */
    fun onSuccess(item: Any? = null)
}