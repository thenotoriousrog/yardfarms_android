package com.appme.yardfarms.backend.persistance

/**
 * Holds various errors that firebase can return for various reasons.
 */
enum class AuthErrorCodes(val error: String) {

    AUTH_EMAIL_ALREADY_IN_USE("auth/email-already-in-use"),
    AUTH_INVALID_EMAIL("auth/invalid-email"),
    AUTH_OPERATION_NOT_ALLOWED("auth/operation-not-allowed"),
    AUTH_WEAK_PASSWORD("auth/weak-password")

}