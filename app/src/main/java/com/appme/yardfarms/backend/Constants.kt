package com.appme.yardfarms.backend

object Constants {

    // General
    const val FILE_PROVIDER_AUTHORITY = "com.appme.yardfarms.provider"

    // Shared Preference TAGs
    const val SHARED_PREFS_TAG = "sharedPrefsTag"
    const val PREFS_HAS_CREATED_ACCOUNT = "hasCreatedAccount" // used to validate that the user actually has an account to use
    const val PREFS_HAS_REGISTERED_BIOMETRICS = "hasRegisteredBiometrics" // used to determine if the user elected to use biometrics
    const val PREFS_USER_CREDENTIALS = "userCredentials" // this holds the user's encrypted credentials. It will be in email:password style
    const val PREFS_IV_ENCODED = "ivEncoded" // holds the iv bytes needed for decrypting stored information for a user
    const val PREFS_HAS_FINISHED_ACCOUNT_SETUP = "hasFinishedAccountSetup" // tells the system that the user has finished account setup
}