package com.appme.yardfarms.backend

/**
 * Represents a user's address credentials
 */
data class Address(val placeId: String?,
                   val placeAddress: String,
                   val unitNumber: String?) {

    constructor() : this("", "", null)

    override fun equals(other: Any?): Boolean {
        if(other == null) { return false }
        if(other === this) { return true }
        if(other !is Address) { return false }

        val otherPlace: Address = other

        return this.placeId == otherPlace.placeId &&
                this.placeAddress == otherPlace.placeAddress &&
                this.unitNumber == otherPlace.unitNumber
    }

    override fun hashCode(): Int {
        var result = 17
        result = 31 * result + placeId.hashCode()
        result = 31 * result + placeAddress.hashCode()
        result = 31 * result + unitNumber.hashCode()
        return result
    }
}