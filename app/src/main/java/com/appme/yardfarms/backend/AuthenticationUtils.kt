package com.appme.yardfarms.backend

import android.content.Context
import android.content.SharedPreferences
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import com.appme.yardfarms.util.edit
import timber.log.Timber
import java.io.FileInputStream
import java.io.IOException
import java.security.GeneralSecurityException
import java.security.KeyStore
import java.security.UnrecoverableKeyException
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec

/**
 * This class is used specifically for biometrics
 */
object AuthenticationUtils {

    private const val ANDROID_KEYSTORE_ALIAS = "AndroidKeyStore"

    /**
     * Generates a general keystore of name "AndroidKeyStore"
     */
    fun generateSecretKey(keystoreAlias: String): SecretKey {

        val keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, keystoreAlias)

        keyGenerator.init(KeyGenParameterSpec.Builder(
            keystoreAlias,
            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
            .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
            .setUserAuthenticationRequired(true)
            .build())

        return keyGenerator.generateKey()
    }

    /**
     * Retrieves a SecretKey
     * @param keyName - the name of the SecretKey to use. Default is AndroidKeyStore
     */
    fun getSecretKey(keyName: String = ANDROID_KEYSTORE_ALIAS): SecretKey {
        val keystore = KeyStore.getInstance(ANDROID_KEYSTORE_ALIAS)
        keystore.load(null)
        return keystore.getKey(keyName, null) as SecretKey
    }


    val cipher: Cipher
        get() = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                    + KeyProperties.BLOCK_MODE_CBC + "/"
                    + KeyProperties.ENCRYPTION_PADDING_PKCS7)

    fun initCipher(cipher: Cipher, mode: Int, secretKey: SecretKey, context: Context, sharedPreferences: SharedPreferences) {
        try {
            val iv: ByteArray
            val ivParams: IvParameterSpec

            if(mode == Cipher.ENCRYPT_MODE) {
                cipher.init(mode, secretKey)
                ivParams = cipher.parameters.getParameterSpec(IvParameterSpec::class.java)
                iv = ivParams.iv
                val encodedIv = String(Base64.encode(iv, Base64.NO_WRAP))

                sharedPreferences.edit { putString(Constants.PREFS_IV_ENCODED, encodedIv) }

            } else {
                val encodedIv = sharedPreferences.getString(Constants.PREFS_IV_ENCODED, null)
                val decoded = Base64.decode(encodedIv, Base64.NO_WRAP)
                iv = decoded
                ivParams = IvParameterSpec(iv)
                cipher.init(mode, secretKey, ivParams)
            }

        } catch (ex: Exception) { // **Note: There are many different exceptions that can be thrown here. Because of this catching the generic Exception object is the best choice until Kotlin has multi-catch.
            Timber.e(ex,"Caught exception when initializing cipher!")
        }
    }

}