package com.appme.yardfarms.backend.persistance

object FirebaseConstants {

    // Children Tags
    const val USERS = "Users"
    const val PROFILE_PICTURES = "profileImage/"

    // User Account Fields
    const val USER_ID = "userId"
    const val USER_NAME = "name"
    const val USER_AGE = "age"
    const val USER_EMAIL = "email"
    const val USER_PROFILE_PICTURE = "profilePicture"
    const val USER_BIO = "bio"
    const val USER_ADDRESS = "address"

}