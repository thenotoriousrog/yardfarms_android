package com.appme.yardfarms

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.ImageView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import timber.log.Timber

/**
 * Custom ImageView that applies a circular border to the image
 */
class CircularImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleTheme: Int = 0
) : ImageView(context, attrs, defStyleTheme) {

    companion object {
        private val DEFAULT_SCALE_TYPE = ScaleType.CENTER_CROP
        private val BITMAP_CONFIG = Bitmap.Config.ARGB_8888
        private const val COLOR_DRAWABLE_DIMENSION = 2
        private const val DEFAULT_BORDER_WIDTH = 0.0f

        @ColorRes
        private const val DEFAULT_BORDER_COLOR = R.color.colorPrimary

        @ColorRes
        private const val DEFAULT_CIRCLE_BACKGROUND_COLOR = R.color.transparent

        private const val DEFAULT_IMAGE_ALPHA = 255
        private const val DEFAULT_BORDER_OVERLAY = false
    }

    private val drawableRect = RectF()
    private val borderRect = RectF()
    private val shaderMatrix = Matrix()
    private val bitmapPaint = Paint()
    private val borderPaint = Paint()
    private val circleBackgroundPaint = Paint()

    /**
     * The color of the border around the image
     */
    @ColorRes
    var borderColor: Int = DEFAULT_BORDER_COLOR
        set(value) {
            field = value

            borderPaint.color = ContextCompat.getColor(context, value)
            invalidate()
        }

    /**
     * The width of the border
     */
    var borderWidth: Float = DEFAULT_BORDER_WIDTH
        set(value) {
            field = value

            borderPaint.strokeWidth = borderWidth
            updateDimensions()
            invalidate()
        }

    @ColorRes
    var circleBackgroundColor = DEFAULT_CIRCLE_BACKGROUND_COLOR
        set(value) {
            field = value
            circleBackgroundPaint.color = ContextCompat.getColor(context, value)
            invalidate()
        }

    var circleImageAlpha: Int = DEFAULT_IMAGE_ALPHA

    private var bitmap: Bitmap? = null

    private var bitmapCanvas: Canvas? = null

    private var drawableRadius: Float = 0.0f
    private var borderRadius: Float = 0.0f

    private var circleColorFilter: ColorFilter? = null

    private var isInitialized = false
    private var shouldRebuildShader = false
    private var isDrawableDirty = false

    /**
     * Tells the CircularImageView to use the border or not
     */
    var useBorderOverlay = false
        set(value) {
            field = value

            updateDimensions()
            invalidate()
        }

    /**
     * Tells the CircularImageView to disable the circular look of the image
     */
    var disableCircularTransformation = false
        set(value) {
            field = value

            if(value) {
                bitmap = null
                bitmapCanvas = null
                bitmapPaint.shader = null
            } else {
                initializeBitmap()
            }

            invalidate()
        }

    init {
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.CircularImageView, defStyleTheme, 0)

        borderWidth = attributes.getDimensionPixelSize(R.styleable.CircularImageView_borderWidth, DEFAULT_BORDER_WIDTH.toInt()).toFloat()
        borderColor = attributes.getColor(R.styleable.CircularImageView_borderColor, DEFAULT_BORDER_COLOR)
        useBorderOverlay = attributes.getBoolean(R.styleable.CircularImageView_borderOverlay, DEFAULT_BORDER_OVERLAY)
        circleBackgroundColor = attributes.getColor(R.styleable.CircularImageView_circleBackgroundColor, DEFAULT_CIRCLE_BACKGROUND_COLOR)

        attributes.recycle()

        setup()
    }

    private fun setup() {
        isInitialized = true

        super.setScaleType(DEFAULT_SCALE_TYPE)
        bitmapPaint.isAntiAlias = true
        bitmapPaint.isDither = true
        bitmapPaint.isFilterBitmap = true
        bitmapPaint.alpha = circleImageAlpha
        bitmapPaint.colorFilter = circleColorFilter

        borderPaint.style = Paint.Style.STROKE
        borderPaint.isAntiAlias = true
        borderPaint.color = ContextCompat.getColor(context, borderColor)
        borderPaint.strokeWidth = borderWidth

        circleBackgroundPaint.style = Paint.Style.FILL
        circleBackgroundPaint.isAntiAlias = true
        circleBackgroundPaint.color = ContextCompat.getColor(context, circleBackgroundColor)

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            outlineProvider = OutlineProvider()
        }
    }

    override fun setScaleType(scaleType: ScaleType?) {
        if(scaleType != DEFAULT_SCALE_TYPE) { // todo: determine if I can allow different scale types for this view!
            throw IllegalArgumentException("ScaleType $scaleType is not supported!")
        }
    }

    override fun setAdjustViewBounds(adjustViewBounds: Boolean) {
        if(adjustViewBounds) {
            throw IllegalArgumentException("AdjustViewBounds is not supported!")
        }
    }

    override fun onDraw(canvas: Canvas?) {
        if(disableCircularTransformation) {
            super.onDraw(canvas)
            return
        }

        if(circleBackgroundColor != DEFAULT_CIRCLE_BACKGROUND_COLOR) {
            canvas?.drawCircle(drawableRect.centerX(), drawableRect.centerY(), drawableRadius, circleBackgroundPaint)
        }

        if(bitmap != null) {
            if(isDrawableDirty && bitmapCanvas != null) {
                isDrawableDirty = false
                val drawable = drawable
                drawable.setBounds(0,0, bitmapCanvas!!.width, bitmapCanvas!!.height)
                drawable.draw(bitmapCanvas!!)
            }

            if(shouldRebuildShader) {
                shouldRebuildShader = false

                val bitmapShader = BitmapShader(bitmap!!, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
                bitmapShader.setLocalMatrix(shaderMatrix)

                bitmapPaint.shader = bitmapShader
            }

            canvas?.drawCircle(drawableRect.centerX(), drawableRect.centerY(), drawableRadius, bitmapPaint)
        }

        if(borderWidth > 0) {
            canvas?.drawCircle(borderRect.centerX(), borderRect.centerY(), borderRadius, borderPaint)
        }
    }

    override fun invalidateDrawable(dr: Drawable) {
        isDrawableDirty = true
        invalidate()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        updateDimensions()
        invalidate()
    }

    override fun setPadding(left: Int, top: Int, right: Int, bottom: Int) {
        super.setPadding(left, top, right, bottom)
        updateDimensions()
        invalidate()
    }

    override fun setPaddingRelative(start: Int, top: Int, end: Int, bottom: Int) {
        super.setPaddingRelative(start, top, end, bottom)
        updateDimensions()
        invalidate()
    }

    override fun setImageBitmap(bm: Bitmap?) {
        super.setImageBitmap(bm)
        initializeBitmap()
        invalidate()
    }

    override fun setImageDrawable(drawable: Drawable?) {
        super.setImageDrawable(drawable)
        initializeBitmap()
        invalidate()
    }

    override fun setImageResource(resId: Int) {
        super.setImageResource(resId)
        initializeBitmap()
        invalidate()
    }

    override fun setImageURI(uri: Uri?) {
        super.setImageURI(uri)
        initializeBitmap()
        invalidate()
    }

    override fun setImageAlpha(alpha: Int) {
        alpha and 0xFF // TODO: determine if I need to remove this or not

        if(alpha == circleImageAlpha) return // do nothing

        circleImageAlpha = alpha

        // This could get called by ImageView construction on devices running API level >= 16
        if(isInitialized) {
            bitmapPaint.alpha = alpha
            invalidate()
        }
    }

    override fun getImageAlpha(): Int {
        return circleImageAlpha
    }

    override fun setColorFilter(cf: ColorFilter?) {
        if(cf == circleColorFilter) return // do nothing

        // This could get called by ImageView construction before initialization finishes on devices running api level <= 19
        if(isInitialized) {
            bitmapPaint.colorFilter = cf
            invalidate()
        }
    }

    override fun getColorFilter(): ColorFilter {
        return circleColorFilter ?: super.getColorFilter()
    }

    private fun getBitmapFromDrawable(drawable: Drawable?): Bitmap? {
        if(drawable == null) return null

        if(drawable is BitmapDrawable){
            return drawable.bitmap
        }

        try {
            val bitmap: Bitmap = if(drawable is ColorDrawable) {
                Bitmap.createBitmap(COLOR_DRAWABLE_DIMENSION, COLOR_DRAWABLE_DIMENSION, BITMAP_CONFIG)
            } else {
                Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, BITMAP_CONFIG)
            }

            val canvas = Canvas(bitmap)
            drawable.setBounds(0,0,canvas.width, canvas.height)
            drawable.draw(canvas)
            return bitmap
        } catch (ex: Exception) {
            Timber.e(ex,"Caught exception attempting to getBitmapFromDrawable")
            return null
        }
    }

    private fun initializeBitmap() {
        bitmap = getBitmapFromDrawable(drawable)

        if(bitmap != null && bitmap!!.isMutable) {
            bitmapCanvas = Canvas(bitmap!!)
        } else {
            bitmapCanvas = null
        }

        if(!isInitialized) return // we're done here

        if(bitmap != null) {
            updateShaderMatrix()
        } else {
            bitmapPaint.shader = null
        }
    }

    private fun updateDimensions() {
        borderRect.set(calculateBounds())
        borderRadius = Math.min((borderRect.height() - borderWidth) / 2.0f, (borderRect.width() - borderWidth) / 2.0f)

        drawableRect.set(borderRect)
        if(!useBorderOverlay && borderWidth > 0) {
            drawableRect.inset(borderWidth - 1.0f, borderWidth - 1.0f)
        }
        drawableRadius = Math.min(drawableRect.height() / 2.0f, drawableRect.width() / 2.0f)

        updateShaderMatrix()
    }

    private fun calculateBounds(): RectF {
        val availableWidth = width - paddingLeft - paddingRight
        val availableHeight = height - paddingTop - paddingBottom

        val sideLength = Math.min(availableWidth, availableHeight)

        val left = paddingLeft + (availableWidth - sideLength) / 2.0f
        val top = paddingTop + (availableHeight - sideLength) / 2.0f

        return RectF(left, top, left + sideLength, top + sideLength)
    }

    private fun updateShaderMatrix() {
        if(bitmap == null) return

        val scale: Float
        var dx: Float = 0.0f
        var dy: Float = 0.0f

        shaderMatrix.set(null)

        val bitmapHeight = bitmap!!.height
        val bitmapWidth = bitmap!!.width

        if(bitmapWidth * drawableRect.height() > drawableRect.width() * bitmapHeight) {
            scale = drawableRect.height() / bitmapHeight.toFloat()
            dx = (drawableRect.width() - bitmapWidth * scale) * 0.5f
        } else {
            scale = drawableRect.width() / bitmapWidth.toFloat()
            dy = (drawableRect.height() - bitmapHeight * scale) * 0.5f
        }

        shaderMatrix.setScale(scale, scale)
        shaderMatrix.postTranslate((dx + 0.5f) + drawableRect.left, (dy + 0.5f) + drawableRect.top)

        shouldRebuildShader = true
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if(disableCircularTransformation) return super.onTouchEvent(event)

        return inTouchableArea(event?.x?.toDouble(), event?.y?.toDouble()) && super.onTouchEvent(event)
    }

    private fun inTouchableArea(x: Double?, y: Double?): Boolean {
        if(borderRect.isEmpty) return true

        if(x == null || y == null) {
            Timber.w("x or y = is null in touchable area! Invalidating touchable area!")
            return false
        }


        return Math.pow(x - borderRect.centerX(), 2.0) + Math.pow(y - borderRect.centerY(), 2.0) <= Math.pow(borderRadius.toDouble(), 2.0)
    }

    private inner class OutlineProvider : ViewOutlineProvider() {
        override fun getOutline(view: View?, outline: Outline?) {
            if(disableCircularTransformation) {
                BACKGROUND.getOutline(view, outline)
            } else {
                val bounds = Rect()
                borderRect.roundOut(bounds)
                outline?.setRoundRect(bounds, bounds.width() / 2.0f)
            }
        }
    }
}