package com.appme.yardfarms

import android.app.Application
import com.appme.yardfarms.backend.persistance.FirebaseHelper
import com.google.android.libraries.places.api.Places
import timber.log.Timber

class YardFarmsApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        FirebaseHelper.initialize()

        if(BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        Places.initialize(this, getString(R.string.google_places_api_key))

    }
}