package com.appme.yardfarms

import android.view.View
import androidx.annotation.StringRes

typealias SnackbarActionClickedListener = (View) -> Unit

/**
 * A type of Snackbar message we are attempting to show to the user
 */
sealed class SnackbarMessage {

    /**
     * Useful for quickly determining which type of Snackbar should be created.
     * Particularly useful when instantiating from Presenters or backend components to prevent
     * UI related code from going into the wrong spot.
     */
    enum class Type {
        PRIORITY,
        SECONDARY,
        ERROR
    }

    companion object {

        /**
         * Quickly generates a general Snackbar message from a given type.
         * Note: this will be a generic Snackbar message without any given customized behavior. Please
         * instantiate a custom object if you need an action to go with it
         */
        fun getFromType(type: Type): SnackbarMessage {
            return when(type) {
                Type.PRIORITY -> Priority()
                Type.SECONDARY -> Secondary()
                Type.ERROR -> Error()
            }
        }
    }

    /**
     * Determines the color of the snackbar
     */
    abstract val snackbarColor: Int

    /**
     * Specifies an action to do from a string resource
     */
    abstract val snackbarAction: Int?

    /**
     * The click listener to assign to capture click actions
     */
    abstract val actionClickListener: SnackbarActionClickedListener?


    /**
     * Normal message that contains information that a user should know about
     */
    data class Priority(
        @StringRes val action: Int? = null,
        val clickListener: SnackbarActionClickedListener? = null
    ) : SnackbarMessage() {

        override val snackbarColor: Int
            get() = R.color.colorComplimentary

        override val snackbarAction: Int?
            get() = action

        override val actionClickListener: SnackbarActionClickedListener?
            get() = clickListener
    }


    /**
     * Non-important message that a user would like to know about but isn't imperative that they know
     */
    data class Secondary(
        @StringRes val action: Int? = null,
        val clickListener: SnackbarActionClickedListener? = null
    ) : SnackbarMessage() {

        override val snackbarColor: Int
            get() = R.color.colorAccent

        override val snackbarAction: Int?
            get() = action

        override val actionClickListener: SnackbarActionClickedListener?
            get() = clickListener
    }

    /**
     * Error message to alert the user that something is not going according to plan
     */
    data class Error(
        @StringRes var action: Int? = null,
        var clickListener: SnackbarActionClickedListener? = null
    ) : SnackbarMessage() {

        override val snackbarColor: Int
            get() = R.color.red

        override val snackbarAction: Int?
            get() = action

        override val actionClickListener: SnackbarActionClickedListener?
            get() = clickListener
    }
}