package com.appme.yardfarms.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore

/**
 * General useful utility classes to be used in androi
 */
object Utils {

    /**
     * Takes a valid uri and retrieves the Bitmap
     */
    fun getBitmapFromUri(uri: Uri, context: Context): Bitmap {
        return if(Build.VERSION.SDK_INT < 28) {
            MediaStore.Images.Media.getBitmap(context.contentResolver, uri)
        } else {
            val source = ImageDecoder.createSource(context.contentResolver, uri)
            ImageDecoder.decodeBitmap(source)
        }
    }

}