package com.appme.yardfarms.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.util.PatternsCompat

/**
 * Used to validate if the current string is an email
 */
fun String.isValidEmail(): Boolean {
    return PatternsCompat.EMAIL_ADDRESS.matcher(this).matches()
}

/**
 * For quickly editing in shared preferences in a lambda block
 */
fun SharedPreferences.edit(function: SharedPreferences.Editor.() -> Unit) {
    val editor = edit()
    editor.function()
    editor.apply()
}

/**
 * Hides keyboard
 */
fun Context.dismissKeyboard(view: View?) {
    view?.let { v ->
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)
    }
}

val Int.dp: Int get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int get() = (this * Resources.getSystem().displayMetrics.density).toInt()

val Float.dp: Float get() = (this / Resources.getSystem().displayMetrics.density)
val Float.px: Float get() = (this * Resources.getSystem().displayMetrics.density)

/**
 * Converts view visiblity to visible
 */
fun View.toVisible() {
    visibility = View.VISIBLE
}

/**
 * Converts view visibility to invisible
 */
fun View.toInvisible() {
    visibility = View.INVISIBLE
}

/**
 * Converts view's visibilty to gone
 */
fun View.toGone() {
    visibility = View.GONE
}

/**
 * Launches to another activity.
 */
fun AppCompatActivity.launch(clazz: Class<out AppCompatActivity>) {
    startActivity(Intent(this, clazz))
}