package com.appme.yardfarms

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.appme.yardfarms.backend.Constants
import com.google.android.material.snackbar.Snackbar

abstract class BaseActivity : AppCompatActivity() {

    /**
     * The id of the layout for this activity
     */
    abstract val layoutResId: Int

    /**
     * The id of the snackbar container to display a snackbar message over
     */
    abstract val snackbarContainerView: View

    val generalSharedPrefs: SharedPreferences
        get() = applicationContext.getSharedPreferences(Constants.SHARED_PREFS_TAG, Context.MODE_PRIVATE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)
    }

    /**
     * Displays a snackbar message to the user
     * @param message - the message from a string resource i.e. R.string.some_string
     * @param length - the length of the message i.e. Snackbar.LENGTH_SHORT, Snackbar.LENGTH_LONG, or Snackbar.LENGTH_INDEFINITE
     * @param messageType - the type of snackbar message that we want to display to the user
     */
    fun displaySnackbarMessage(@StringRes message: Int, length: Int, messageType: SnackbarMessage) {
        val snackbar = Snackbar.make(snackbarContainerView, message, length)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(ContextCompat.getColor(this, messageType.snackbarColor))

        if(messageType.snackbarAction != null && messageType.actionClickListener != null) {
            snackbar.setAction(messageType.snackbarAction!!, messageType.actionClickListener)
        }


        snackbar.show()
    }

    /**
     * Shows a toast message. Used when Snackbar is not possible or the best choice
     * @param message - the message to display to users
     * @param length - the length of time to show the message to the user
     */
    fun displayToastMessage(@StringRes message: Int, length: Int) {
        Toast.makeText(this, message, length).show()
    }
}